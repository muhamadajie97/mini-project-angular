import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {
  constructor(private router: Router ){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const getToken = localStorage.getItem('token');
      const getRole = localStorage.getItem('role');
      const activePath = state.url

      switch (activePath) {
        case "/register":
        case "/login":
          getToken && getRole == 'member' ? this.router.navigate(['user']) : true;
          return true;
        case "/login":
        case "/admin":
          getToken && getRole == 'admin' ? true : this.router.navigate(['admin']);
          return true;
        default:
          getToken ? true : this.router.navigate(['login']);
          return true;
      }
  }
}
