export const courses = [
    {
        images: 'https://blog.geekhunter.com.br/wp-content/uploads/2019/10/Um-overview-sobre-o-framework-Angular.jpg',
        title: 'Angular JS Course',
        type: 'Angular',
        desc: `With Angular, you're taking advantage of a platform that can scale from single-developer projects to enterprise-level applications.`
    },
    {
        images: 'https://glints.com/id/lowongan/wp-content/uploads/2020/10/logo-reactjs.jpg',
        title: 'React JS Course',
        type: 'React',
        desc: `React has been designed from the start for gradual adoption, and you can use as little or as much React as you need.`
    },
    {
        images: 'https://miro.medium.com/max/1000/1*htbUdWgFQ3a94PMEvBr_hQ.png',
        title: 'Next JS Course',
        type: 'Next',
        desc: `Next.js is an open-source development framework built on top of Node.js.`
    }
]
