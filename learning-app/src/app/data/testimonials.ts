export const testimonials = [
    {
        imageUrl: 'https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(10).webp',
        name: 'Maria Kate',
        job: 'Photographer',
        text: '"Saya khusus mendedikasikan waktu saya untuk belajar ngoding. Di Dicoding belajarnya step by step, library-nya up-to-date. Kalau ada eror, nggak bingung. Di sini saya juga belajar untuk nggak asal coding. CV pun jadi bagus. Saya jadi percaya diri."'
    },
    {
        imageUrl: 'https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(32).webp',
        name: 'John Doe',
        job: 'Web Developer',
        text: '"Banyak sekali materi yang bagus di sini. Pengalaman saya, dengan kelas Android di Dicoding saya jadi PD untuk ambil bidang Android di Tokopedia dan disetujui. Bahkan sebelum wisuda kuliah, saya telah berhasil lulus seleksi dan diterima kerja di unicorn tersebut."'
    },
    {
        imageUrl: 'https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(1).webp',
        name: 'Anna Deynah',
        job: 'UX Designer',
        text: '"Yang kudapat dari kelas ini adalah teknik belajar yang terstruktur dan komunitasnya yang bagus. Dalam waktu 2 tahun, aku mempersiapkan diri demi pindah karir. Sebelumnya geolog di perusahaan migas, kini aku lead developer di bank nasional!"'
    }
]