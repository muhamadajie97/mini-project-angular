import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from './services/profile.service';
import { interval } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'learning-app';
  timer = interval(500)

  data: any
  nama: string = ''
  cekUrlPrent: string = ''

  constructor(private router: Router, private profileService: ProfileService) { }

  ngOnInit(): void {
    this.timer.subscribe(() => this.show())
  }

  getData() {
    const getProfile = this.profileService.getUser()

    getProfile.subscribe(user => {
      this.data = user.data
    })
  }

  cekToken = false

  show() {
    let cek = localStorage.getItem('token');
    if(cek !== null){
      this.cekToken = true
      this.getData()
    }
  }

  logout() {
    this.cekToken = false
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
