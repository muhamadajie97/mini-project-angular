import { Component, OnInit } from '@angular/core';
import { testimonials } from 'src/app/data/testimonials';
import { courses } from 'src/app/data/courses';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  datas = testimonials
  courses = courses

}
