import { Component, OnInit, EventEmitter } from '@angular/core';
import { ContentsService } from 'src/app/services/contents.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  constructor(private contentsService: ContentsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getContentsLearning()
  }

  datas: any
  embedVideoId: string = ''
  enteredSearchValue: string = ''

  searchTextChange: EventEmitter<string> = new EventEmitter<string>()

  onSearchTextChange() {
    this.searchTextChange.emit(this.enteredSearchValue)
  }

  getContentsLearning() {
    const contents = this.contentsService.getContents();

    contents.subscribe((content: any) => {
      this.datas = content.data;
    })
  }

  getEmbedVideo(id: string) {
    this.embedVideoId = id
  }
}
