import { Component, OnInit, EventEmitter } from '@angular/core';
import { ContentsService } from 'src/app/services/contents.service';
import { UsersService } from 'src/app/services/users.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { courses } from 'src/app/data/courses';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  form = new FormGroup({
    title: new FormControl('', Validators.required),
    desc: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    video: new FormControl('', Validators.required),
    images: new FormControl('', Validators.required),
    fileSource: new FormControl('', Validators.required)
  });

  get title() { return this.form.get('title'); }
  get desc() { return this.form.get('desc'); }
  get type() { return this.form.get('type'); }
  get video() { return this.form.get('video'); }
  get images() { return this.form.get('images'); }

  constructor(private contentsService: ContentsService, private usersService: UsersService, private route: ActivatedRoute) { }

  cekUrlChild: string = ''

  ngOnInit(): void {
    this.getUserContents()
    this.totalUsers()
    this.cekUrlPath()
  }

  cekUrlPath() {
    this.cekUrlChild = this.route.snapshot.routeConfig?.path!
  }

  datas: any
  courseDatas = courses
  total: any
  totalVideos: any
  enteredSearchValue: string = ''
  embedVideoId: string = ''

  searchTextChange: EventEmitter<string> = new EventEmitter<string>()

  onSearchTextChange() {
    this.searchTextChange.emit(this.enteredSearchValue)
  }

  getUserContents() {
    const userContents = this.contentsService.getContents();

    userContents.subscribe((content: any) => {
      this.datas = content.data;
      this.totalVideos = content.data.length
    })
  }

  getEmbedVideo(id: string) {
    this.embedVideoId = id
  }

  totalUsers() {
    const totalUsersActive = this.usersService.getUsers();

    totalUsersActive.subscribe((content: any) => {
      this.total = content.data.length;
    })
  }

  clear() {
    this.form.reset()
  }

  loadingSubmit = true

  onFilechange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.patchValue({
        fileSource: file
      });
    }
  }

  addVideo() {
    this.loadingSubmit = false;
    const formData = new FormData();
    formData.append('title', this.form.get('title')?.value ?? '')
    formData.append('desc', this.form.get('desc')?.value ?? '')
    formData.append('type', this.form.get('type')?.value ?? '')
    formData.append('video', this.form.get('video')?.value ?? '')
    formData.append('images', this.form.get('fileSource')?.value ?? '');

    const addContents = this.contentsService.addContent(formData);

    if(this.form.value.title === '' && this.form.value.desc === '' && this.form.value.type === '' && this.form.value.video === '') {
      alert("Please enter all form values")
      this.loadingSubmit = true;
    } else {
      addContents.subscribe((content: any) => {
        if (content) {
          this.getUserContents()
          this.clear()
          this.loadingSubmit = true;
          alert("Success add video")
        }
      })
    };
  }

  btnDelete(id: string){
    this.contentsService.deleteContent(id)
      .subscribe((res) => {
        this.datas = this.datas.filter((item: any) => item.id !== id)
        alert("Success deleted video")
      })
  }

  dataEdit: any
  filtering: any;

  editU = {
    title: '',
    desc: '',
    type: '',
    video: '',
  }

  btnEdit(id: string){
    this.filtering = this.datas.filter((item: any) => item.id == id);
    this.editU = this.filtering[0]
  }

  btnSaveChange(editU: NgForm){
    const editUser = this.contentsService.editContent(this.filtering[0].id, editU.form.value)
    editUser.subscribe(res => {
      if(res) {
        this.getUserContents()
        alert("Selamat data berhasil diubah");
      }
    })
  }

  filterCourse: any
  selectCourse(type: string) {
    this.filterCourse = this.datas.filter((item: any) => item.type == type);
    this.datas = this.filterCourse
  }
}
