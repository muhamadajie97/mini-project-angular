import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
  })

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  loadingSubmit = true

  clear() {
    this.loginForm.reset()
  }

  submit() {
    const { email, password } = this.loginForm.value;
    this.loadingSubmit = false;

    if(email !== '' && password !== '') {
      this.authService.login({
        email,
        password
      }).subscribe((data:any) => {
        if(data.message === 'success' && data.role === 'admin' || data.role === 'superadmin') {
          this.router.navigate(['admin'])
          localStorage.setItem('token',data.token);
          localStorage.setItem('role', data.role)
        }else if(data.message === 'success' && data.role === 'member') {
          this.router.navigate(['user'])
          localStorage.setItem('token',data.token);
          localStorage.setItem('role', data.role)
        } else {
          this.loadingSubmit = true;
          alert('Please enter a valid email or password');
        }
      })
    } else {
      this.loadingSubmit = true;
      alert('Please enter a valid email or password');
    }
  }
}
