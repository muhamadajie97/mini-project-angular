import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from './helpers/permission.guard';

// Pages Components
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { UserPageComponent } from './components/user-page/user-page.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { LoginPageComponent } from './components/auth/login-page/login-page.component';
import { RegisterPageComponent } from './components/auth/register-page/register-page.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';

const routes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'user', component: UserPageComponent, canActivate: [PermissionGuard]},
  {path: 'admin', component: AdminPageComponent, canActivate: [PermissionGuard]},
  {path: 'login', component: LoginPageComponent, canActivate: [PermissionGuard]},
  {path: 'register', component: RegisterPageComponent, canActivate: [PermissionGuard]},
  {path: '**', component: NotFoundPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
