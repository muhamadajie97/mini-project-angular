import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
		private http: HttpClient,
	) { }

	urlApi = 'http://localhost:3001/user/profile'

	getUser() {
    return this.http.get(this.urlApi, {
      headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }}).pipe(map((res: any) => {
      return res;
    }));
	}
}
