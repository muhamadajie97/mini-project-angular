import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContentsService {
  urlApi = 'http://localhost:3001/content'

  constructor(private http: HttpClient,) { }

  getContents() {
    return this.http.get(this.urlApi)
	}

  addContent(payload: any) {
    return this.http.post(this.urlApi, payload, {
      headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }})
  }

  editContent(id: string, payload: any) {
    return this.http.put(`${this.urlApi}/${id}`, payload, {
      headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }})
  }

  deleteContent(id: string) {
    return this.http.delete(`${this.urlApi}/${id}`, {
      headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }})
  }
}
