import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(
		private http: HttpClient,
	) { }

	urlApi = 'http://localhost:3001/user'

	login(data: any) {
		return this.http.post(`${ this.urlApi }/login`, data)
	}

	register(payload: any) {
		return this.http.post(`${ this.urlApi }/register`, payload)
	}
}
