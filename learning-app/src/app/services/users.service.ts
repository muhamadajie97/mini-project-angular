import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  urlApi = 'http://localhost:3001/user'

  constructor(private http: HttpClient,) { }

  getUsers() {
    return this.http.get(this.urlApi)
	}
}
